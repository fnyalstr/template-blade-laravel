<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class indexController extends Controller
{
    public function home()
    {
        return view('welcome');
    }
    public function register()
    {
        return view('halaman.register');
    }
    public function welcome(request $request)
    {
        $firstname= $request->firstname;
        $lastname= $request->lastname;

        return view('halaman.home',compact('firstname','lastname'));
    }
}
